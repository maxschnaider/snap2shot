import { PropsWithChildren } from 'react';
import styled from 'styled-components';
import { Header } from './components';

import { GlobalStyle } from './config/theme';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
  max-width: 100vw;
`;

export const App = ({ children }: PropsWithChildren) => (
  <>
    <GlobalStyle />
    <Wrapper>
      <Header />
      {children}
    </Wrapper>
  </>
);
