import { PropsWithChildren } from 'react';
import { ThemeProvider } from 'styled-components';
import { dark } from './config/theme';
import { MetaMaskProvider } from './context';

export const Root = ({ children }: PropsWithChildren) => (
  <ThemeProvider theme={dark}>
    <MetaMaskProvider>{children}</MetaMaskProvider>
  </ThemeProvider>
);
