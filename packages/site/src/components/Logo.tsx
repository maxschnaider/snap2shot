export const Logo = ({ color, size }: { color: string; size: number }) => (
  <svg
    width={size}
    height={size}
    viewBox={`0 0 125 125`}
    fill="white"
    xmlns="http://www.w3.org/2000/svg"
  >
    <text y={size * 3} font-size={size * 3.3}>
      ⚡️
    </text>
  </svg>
);
