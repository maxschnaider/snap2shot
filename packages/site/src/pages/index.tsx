import { useContext } from 'react';
import styled from 'styled-components';
import { MetamaskActions, MetaMaskContext } from '../context';
import {
  connectSnap,
  getSnap,
  invokeTimeline,
  shouldDisplayReconnectButton,
} from '../utils';
import {
  ConnectButton,
  InstallFlaskButton,
  ReconnectButton,
  Button,
  Card,
} from '../components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  margin-top: 7.6rem;
  margin-bottom: 7.6rem;
  ${({ theme }) => theme.mediaQueries.small} {
    padding-left: 2.4rem;
    padding-right: 2.4rem;
    margin-top: 2rem;
    margin-bottom: 2rem;
    width: auto;
  }
`;

const Heading = styled.h1`
  margin-top: 0;
  margin-bottom: 2.4rem;
  text-align: center;
`;

const Span = styled.span`
  color: ${(props) => props.theme.colors.primary.default};
`;

const Subtitle = styled.p`
  font-size: ${({ theme }) => theme.fontSizes.large};
  font-weight: 500;
  margin-top: 0;
  margin-bottom: 0;
  ${({ theme }) => theme.mediaQueries.small} {
    font-size: ${({ theme }) => theme.fontSizes.text};
  }
`;

const CardContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  max-width: 64.8rem;
  width: 100%;
  height: 100%;
  margin-top: 1.5rem;
`;

const Notice = styled.div`
  background-color: ${({ theme }) => theme.colors.background.alternative};
  border: 1px solid ${({ theme }) => theme.colors.border.default};
  color: ${({ theme }) => theme.colors.text.alternative};
  border-radius: ${({ theme }) => theme.radii.default};
  padding: 2.4rem;
  margin-top: 2.4rem;
  max-width: 60rem;
  width: 100%;

  & > * {
    margin: 0;
  }
  ${({ theme }) => theme.mediaQueries.small} {
    margin-top: 1.2rem;
    padding: 1.6rem;
  }
`;

const ErrorMessage = styled.div`
  background-color: ${({ theme }) => theme.colors.error.muted};
  border: 1px solid ${({ theme }) => theme.colors.error.default};
  color: ${({ theme }) => theme.colors.error.alternative};
  border-radius: ${({ theme }) => theme.radii.default};
  padding: 2.4rem;
  margin-bottom: 2.4rem;
  margin-top: 2.4rem;
  max-width: 60rem;
  width: 100%;
  ${({ theme }) => theme.mediaQueries.small} {
    padding: 1.6rem;
    margin-bottom: 1.2rem;
    margin-top: 1.2rem;
    max-width: 100%;
  }
`;

const Index = () => {
  const [state, dispatch] = useContext(MetaMaskContext);

  const handleConnectClick = async () => {
    try {
      await connectSnap();
      const installedSnap = await getSnap();

      dispatch({
        type: MetamaskActions.SetInstalled,
        payload: installedSnap,
      });
    } catch (e) {
      console.error(e);
      dispatch({ type: MetamaskActions.SetError, payload: e });
    }
  };

  const handleExploreClick = async () => {
    try {
      await invokeTimeline();
    } catch (e) {
      console.error(e);
      dispatch({ type: MetamaskActions.SetError, payload: e });
    }
  };

  return (
    <Container>
      <Heading>
        Welcome to the <Span>⚡️ Snap²shot</Span>
      </Heading>
      <Subtitle>
        Have all your{' '}
        <a href="https://snapshot.org/" target="_blank">
          snapshot.org
        </a>{' '}
        spaces timeline, proposals, notifications and voting experience directly
        in Metamask!
      </Subtitle>
      <CardContainer>
        {state.error && (
          <ErrorMessage>
            <b>An error happened:</b> {state.error.message}
          </ErrorMessage>
        )}
        {!state.isFlask && (
          <Card
            content={{
              title: 'Install',
              description:
                'Snaps is pre-release software only available in MetaMask Flask, a canary distribution for developers with access to upcoming features.',
              button: <InstallFlaskButton />,
            }}
            fullWidth
          />
        )}
        {!state.installedSnap && (
          <Card
            content={{
              title: 'Connect',
              description:
                'Get started by connecting to and installing the snap.',
              button: (
                <ConnectButton
                  onClick={handleConnectClick}
                  disabled={!state.isFlask}
                />
              ),
            }}
            disabled={!state.isFlask}
          />
        )}
        {shouldDisplayReconnectButton(state.installedSnap) && (
          <Card
            content={{
              title: 'Reconnect',
              description:
                'While connected to a local running snap this button will always be displayed in order to update the snap if a change is made.',
              button: (
                <ReconnectButton
                  onClick={handleConnectClick}
                  disabled={!state.installedSnap}
                />
              ),
            }}
            disabled={!state.installedSnap}
          />
        )}
        <Card
          content={{
            title: 'Explore Timeline',
            description: 'All your DAO memberships & activity in-wallet.',
            button: (
              <Button
                onClick={handleExploreClick}
                disabled={!state.installedSnap}
              >
                👀 Explore
              </Button>
            ),
          }}
          disabled={!state.installedSnap}
          // fullWidth={
          //   state.isFlask &&
          //   Boolean(state.installedSnap) &&
          //   !shouldDisplayReconnectButton(state.installedSnap)
          // }
        />
        <Card
          content={{
            title: 'Vote on Proposals',
            description: 'Better decisions with a better voting experience.',
            button: (
              <Button
                onClick={handleExploreClick}
                disabled={!state.installedSnap}
              >
                ✅ Vote
              </Button>
            ),
          }}
          disabled={!state.installedSnap}
        />
        <Card
          content={{
            title: 'Notifications',
            description: 'Never miss a decision with web push notifications.',
            button: (
              <Button
                onClick={handleExploreClick}
                disabled={!state.installedSnap}
              >
                🔔 Notify
              </Button>
            ),
          }}
          disabled={!state.installedSnap}
        />
        <Notice>
          <p>
            Built with ❤️‍🔥 by{' '}
            <a href="https://twitter.com/MaxSchnaider" target="_blank">
              Max Schnaider
            </a>{' '}
            🇺🇦 during{' '}
            <a
              href="https://daoglobalhackathon.hackerearth.com/"
              target="_blank"
            >
              DAO Global Hackathon 2023
            </a>
          </p>
        </Notice>
      </CardContainer>
    </Container>
  );
};

export default Index;
