# snap2shot

The `Hello, world!` of MetaMask Snaps for [snapshot.org](https://snapshot.org): explore DAO spaces timeline and vote for proposals.

Built with ❤️‍🔥 by [Max Schnaider 🇺🇦](https://twitter.com/MaxSchnaider) during [DAO Global Hackathon 2023](https://daoglobalhackathon.hackerearth.com/).

## Notes

- Babel is used for transpiling TypeScript to JavaScript, so when building with the CLI,
  `transpilationMode` must be set to `localOnly` (default) or `localAndDeps`.
